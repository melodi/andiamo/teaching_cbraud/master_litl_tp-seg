# Master LiTL - Tutoriel Segmentation Discursive

laura.riviere@irit.fr

3 séances de 2h

## Lectures :
### À lire avant le TP

- Adaptation du manuel ANNODIS aux écrits scolaires:  
Annexes A et B du mémoire de Lala, M.(2017). Consulter les annexes D et E. Pour aller plus loin, vous pouvez lire le manuscrit du mémoire : *Outils linguistiques pour l'analyse de la cohérence et de la cohésion dans les textes d'enfants.*  https://dante.univ-tlse2.fr/s/fr/item/4500)  
- Article lié au projet E-CALM:  
Bras, M., Vieu, L., Joret, M., Pépin-Boutin, A., Poujade, C. & Roze, C. (2021). Vers un corpus de textes d’élèves annoté en relations de discours. *Langue française*, 211, 115-129. https://doi-org.gorgone.univ-toulouse.fr/10.3917/lf.211.0115  
- Article lié à ToNy:  
Muller, P., Braud, C., & Morey, M. (2019). ToNy: Contextual embeddings for accurate multilingual discourse segmentation of full documents. In *Proceedings of the Workshop on Discourse Relation Parsing and Treebanking* 2019 (pp. 115-124). Association for Computational Linguistics.
- Article lié à DISCUT:
Morteza Kamaladdini Ezzabady, Philippe Muller, and Chloé Braud. 2021. Multi-lingual Discourse Segmentation and Connective Identification: MELODI at Disrpt2021. In *Proceedings of the 2nd Shared Task on Discourse Relation Parsing and Treebanking* (DISRPT 2021), pages 22–32, Punta Cana, Dominican Republic. Association for Computational Linguistics.

### Autre références : Rappel/Approfondissement
- Manuel d'annotation RST DT :  
Carlson, L., & Marcu, D. (2001). Discourse tagging reference manual. *ISI Technical Report ISI-TR-545*, 54(2001), 56. https://www.isi.edu/~marcu/discourse/tagging-ref-manual.pdf 
- Séminaire Chloé Braud (Thématiques actuelles de la recherche en TAL):  
AnDiAMO: Analyzing Discourse Automatically, with Multiple Objectives. http://w3.erss.univ-tlse2.fr/UETAL/2023-2024/Braud.pdf


## Déroulé du TP

- Rappels théoriques : discours, segmentation discursive, relations discursives...
- Présentation : campagne DISRPT, modèles de segmentation état de l'art (Discut, DiscoDisco).
- Analyse de données segmentées en différentes langues (chinois, français, anglais..) différences entres gold et prédictions, données issues de DISRPT 2021. 
- Utilisation d’un modèle pré-entrainé (ToNy) sur de nouvelles données, en utilisant données ECALM (français)
    - Transformation du format conll-12 de ECALM au format conll de DISRPT pouvant être pris en entrée de DisCut.
    - Prédictions automatiques avec Tony via DisCut.
    - Recollage des métadata de départ.
- Évaluation de l’annotation automatique
- Ré-annotation manuelle (binomes, accord inter ou categorisations des difficutés)
- Fine-tuning en utilisant les nouvelles données annotées et évaluation des nouvelles prédictions automatiques.

## Evaluation

(Binômes ?)
Rédaction d’un compte-rendu sur:
- Prise en main de DisCut
- Expérience d’annotation
- Evaluation des erreurs de DisCut


